{
module Funcs_Parser_Lazy ( runParser ) where

import Funcs_Lexer

import Data_Lazy
}

%name parser p
%tokentype { Token }

%token 
            ';'  { Tsemicolon }
            '('  { TopenB }
            ')'  { TcloseB }
            '['  { TopenSB }
            ']'  { TcloseSB }
            '{'  { TopenCB }
            '}'  { TcloseCB }
            ':'  { Ttowdots }
            ','  { Tcomma }
            EQUAL { Tequal }
            AND   { Tand }
            OR    { Tor }
            '='   { Tassing }
            '+'   { Tadd }
            '*'   { Tmul }
            '-'   { Tsub }
            '/'   { Tdiv }
            '>'   { Tgt }
            '<'   { Tlt  }
            '!'   { Tnot }
            TRUE  { Ttrue }
            FALSE { Tfalse }
            INT_KW   { Tint }
            REAL_KW  { Treal }
            BOOL_KW  { Tbool }
            CHAR_KW  { Tchar }
            IF       { Tif }
            THEN     { Tthen }
            ELSE     { Telse }
            WHILE    { Twhile }
            INPUT    { Tinput }
            PRINT    { Tprint }
            IDENT    { TIdent $$ }
   	    INT_DEN  { TintVal $$ }
   	    REAL_DEN { TrealVal $$ }

%%

p    : defs
     { C_RootProd_1 $1 (C_CSizePP_1 40) }

defs : def ';' defs
     { C_Defs2_1 $1 $3 }
     |  
     { C_NoDefs_1 }

def  : type name
     { C_Vardecl_1 $1 $2 }
     | type name '[' INT_DEN ']'
     { C_Arraydecl_1 $1 $2 (toInteger $4) }
     | name '(' lstformpars ')' ':' type '{' stats '}'
     { C_Declfunc_1 $6 $1 $3 $8 }  

type : INT_KW
     { C_Inttype_1 }
     | REAL_KW
     { C_Realtype_1 }
     | BOOL_KW
     { C_Booltype_1 }
     | CHAR_KW 
     { C_Chartype_1 }
 

lstformpars : 
            { C_Emptyformpars_1 }
            | formpars
            { $1 } 

formpars : formpar ',' formpars
         { C_Lstformpars_1 $1 $3 }
         | formpar 
         { C_Lstformpars_1 $1 C_Emptyformpars_1 }


formpar : type name
        { C_Declformpar_1 $1 $2 }

stats : stat ';' stats
      { C_Lststats_1 $1 $3 }
      |
      { C_Emptystat_1 }


stat : type name
     { C_LocalDecl_1 $1 $2 }
     | name '=' exp
     { C_Assign_1 $1 $3 }
     | arrayuse '=' exp
     { C_ArrAssign_1 $1 $3 }
     | name '(' lstactpars ')'
     { C_Funccall_1 $1 $3 }
     | WHILE exp '{' stats '}'
     { C_While_1 $2 $4 }
     | IF exp THEN '{' stats '}' ELSE '{' stats '}' 
     { C_If_t_e_1 $2 $5 $9 }
     | INPUT name
     { C_Input_1 $2 }
     | PRINT exp
     { C_Print_1 $2 }

exp : fac
     { C_Factor_1 $1 }
     | exp '+' exp
     { C_AddExp_1 $1 $3 }
     | exp '-' exp
     { C_SubExp_1 $1 $3 }
     | exp '*' exp
     { C_MulExp_1 $1 $3 }
     | exp '/' exp
     { C_DivExp_1 $1 $3 }
     | exp AND exp
     { C_AndExp_1 $1 $3 }
     | exp OR exp
     { C_OrExp_1 $1 $3 }
     | exp EQUAL exp
     { C_EqExp_1 $1 $3 }
     | exp '>' exp
     { C_GTExp_1 $1 $3 }
     | exp '<' exp
     { C_LTExp_1 $1 $3 }
     | '!' exp
     { C_NotExp_1 $2 }
     | '-' exp
     { C_MinExp_1 $2 }

fac : INT_DEN
     { C_IntConst_1 (toInteger $1) }
     | REAL_DEN
     { C_RealConst_1 $1 }
     | TRUE
     { C_BoolConst_1 True }
     | FALSE
     { C_BoolConst_1 False }
     | arrayuse
     { C_ArrayConst_1 $1 }
     | name
     { C_CNIdent_1 $1 }
     | name '(' actpars ')'
     { C_Funcinv_1 $1 $3 }
     | '(' exp ')'
     { C_Expr_1 $2 }

arrayuse : name '[' exp ']'
         { C_ArrayInd_1 $1 $3 }

lstactpars : 
           { C_Emptyactpars_1 }
           | actpars
           { $1 }


actpars : exp ',' actpars
        { C_Lstactpars_1 $1 $3 }
        | exp
        { C_Lstactpars_1 $1 C_Emptyactpars_1 }


name : IDENT
     { Ident $1 }


{

-- happyError :: [Token] -> a
happyError _ = error ("Parse error in line \n")



-- runParser :: String -> Error
runParser = parser . lexer


testParser :: String -> IO()
testParser filename 
 = do s <- readFile filename
      let tks = lexer s
      putStr (show tks)
      let ast = parser tks
      putStr (show ast)
      return () -- (scanner inp) 


}

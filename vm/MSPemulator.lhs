

\begin{code}

{-# LANGUAGE BangPatterns #-}

module MSPemulator where

import Data.Char
import Data.List
import Data_Lazy
import LrcPrelude

import Control.Concurrent.MVar

\end{code}


\section{Stack}


\begin{code}

type Stack = [Integer]

emptyStack :: Stack
emptyStack = []

push :: Integer -> Stack -> Stack
push v stack = v : stack

pop :: Stack -> Stack
pop  []     = error "Pop of an empty stack"
pop  (h:t)  = t

top :: Stack -> Integer
top []      = error "Top of an empty stack!"
top (h:t)   = h

\end{code}

\section{Symbols}



\begin{code}

type Symbol = (String        -- name
              ,Integer       -- size
              ,Integer)      -- address in the heap

-- lookupSymb :: String -> [Symbol] -> Symbol
lookupSymb n []     =  error "Symbol not in the heap!"
lookupSymb n (h:t)  |  n == n'    = h
                    |  otherwise  = lookupSymb n t 
  where (n',s,a) = h
\end{code}  


\section{Heap}


\begin{code}

-- allocMem :: [b] -> Int -> [b]
allocMem mem nbytes = mem ++ (map (\v -> 0) [1..nbytes])


-- allocMem mem nbytes = mem ++ (replicate nbytes 0)


--updateMemAddress :: [a] -> Int -> a -> [a]
updateMemAddress (h:t) 0 v = v:t
updateMemAddress (h:t) i v = h : updateMemAddress t (i-1) v


-- getMemAddress :: [a] -> Int -> a
getMemAddress mem address = ith mem  address

ith (h:t) 0 = h
ith (h:t) n = ith t (n-1)

\end{code}

\section{helpers}
\begin{code}

ternary :: Bool -> (t, t) -> t
ternary a (b, c) = if a then b else c

updateMethodCount :: [(String, Int)] -> (Int -> Int) -> [(String, Int)]
updateMethodCount ((m, c):t) f = (m, f c):t 

\end{code}

\begin{code}

type State   = ([Integer], [Integer], [Symbol])
type Methods = [(String, Int)] -- [(methodName, recursiveCalls)]


hamsp :: Code -> IO State
hamsp prog = do methodsMVar <- newEmptyMVar
                let! methods = [("main", 0), ("", 0)] 
                !p <- putMVar methodsMVar methods
                runMSP prog prog (emptyStack, [], []) methodsMVar

runMspProg :: Code -> MVar Methods -> IO State
runMspProg prog methods = runMSP prog prog (emptyStack, [], []) methods

runMSP :: Code -> Code -> State -> MVar Methods -> IO State
runMSP prog [] state methods = return state

runMSP prog (Halt :t) state@(stack,heap,symbs) methods = return state

\end{code}



\begin{code}

runMSP prog (Data :t) state methods =  runMSP prog t state methods

runMSP prog (Cod :t) state methods =  runMSP prog t state methods

runMSP prog (Var n a s ty : t) (stack,heap,symbs) methods = runMSP prog t (stack,heap',symbs') methods
  where  symbs' = (n,a,s) : symbs
         heap'  = allocMem heap s
  

\end{code}



\section{Stack Instructions}

\begin{code}
runMSP prog p@(Pushi i:t) state methods = 
    do  debug p state
        let (stack,heap,symbs)  =  state
        let state'              =  (push i stack , heap , symbs)
        runMSP prog t state' methods

runMSP prog p@(Pusha n i:t) state methods = 
    do  debug p state
        let (stack,heap,symbs)  =  state
        let  (n',a,s)           =  lookupSymb (n++"_"++show i) symbs  
        let  state'             =  (push a stack , heap , symbs)
        runMSP prog t state' methods

runMSP prog p@(Store :t) state methods =
    do  debug p state 
        let (stack,heap,symbs)  =  state
        let v                   =  top stack
        let stack'              =  pop stack
        let address             =  top stack'
        let heap'               =  updateMemAddress heap address v
        runMSP prog t (pop stack' , heap' , symbs) methods

runMSP prog p@(Load :t) state methods =
    do  debug p state 
        let (stack,heap,symbs)  =  state
        let address             =  top stack
        let v                   =  getMemAddress heap address
        let stack'              =  push v (pop stack)
        runMSP prog t (stack' , heap , symbs) methods

\end{code}



\paragraph{Calling Functions}


\begin{code}

runMSP prog p@(Call n:t) state methods = 
    do  debug p state
        !m <- takeMVar methods
        let f = (\x -> x + 1)
        let m' = ternary (fst (m!!0) == n) (updateMethodCount m f, (n, 1):m) 
        !p <- putMVar methods m'
        let (stack,heap,symbs)  =  state
        let pc       =  npc (length prog)  (length t)
        let stack'   =  push pc stack
        jmp prog n (stack',heap,symbs) methods


runMSP prog p@(Ret : t) state methods =  
    do  debug p state
        !m <- takeMVar methods
        let f = (\x -> x - 1)
        let m' = ternary (snd (m!!0) > 0) (updateMethodCount m f, tail m)
        !p <- putMVar methods m'
        let (stack,heap,symbs)  =  state
        let v                   =  top stack
        let stack'              =  pop stack
        let prog'               =  drop (toInt' v) prog 
        runMSP prog prog' (stack',heap,symbs) methods
 
\end{code}



\paragraph{IO Instructions}

\begin{code}
runMSP prog p@(IOut :t) (stack,heap,symbs) methods = 
    do  debug p (stack,heap,symbs) 
        putStrLn (show $ top stack)
        runMSP prog t (pop stack , heap , symbs) methods

runMSP prog p@(IOutC :t) (stack,heap,symbs) methods = 
    do  debug p (stack,heap,symbs) 
        putChar (chr $ fromInteger (top stack))
        runMSP prog t (pop stack , heap , symbs) methods


runMSP prog (IIn :t) (stack,heap,symbs) methods = 
    do  putStrLn ("Introduza um inteiro:")
        v <- getLine          
        let v' = (read v):: Integer

        runMSP prog t (push v' stack , heap , symbs) methods
\end{code}


\paragraph{Arithmetic Instructions}

\begin{code}
runMSP prog p@(Add :t) (stack,heap,symbs) methods = 
    do  debug p (stack,heap,symbs) 
        let (op1:stack')  =  stack
        let (op2:stack'') =  stack'
        let stack'''      =  push (op1 + op2) stack''
        runMSP prog t (stack''' , heap , symbs) methods

runMSP prog p@(Adda :t) (stack,heap,symbs) methods = 
    do  debug p (stack,heap,symbs) 
        let (op1:stack')  =  stack
        let (op2:stack'') =  stack'
        let stack'''      =  push (op1 + op2) stack''
        runMSP prog t (stack''' , heap , symbs) methods


runMSP prog p@(Mul :t) (stack,heap,symbs) methods = 
    do  debug p (stack,heap,symbs) 
        let (op1:stack')  =  stack
        let (op2:stack'') =  stack'
        let stack'''      =  push (op1 * op2) stack''
        runMSP prog t (stack''' , heap , symbs) methods

runMSP prog p@(Sub :t) (stack,heap,symbs) methods = 
    do  debug p (stack,heap,symbs) 
        let (op1:stack')  =  stack
        let (op2:stack'') =  stack'
        let stack'''      =  push (op2 - op1) stack''
        runMSP prog t (stack''' , heap , symbs) methods

runMSP prog p@(Div :t) (stack,heap,symbs) methods = 
    do  debug p (stack,heap,symbs) 
        let (op1:stack')  =  stack
        let (op2:stack'') =  stack'
        let stack'''      =  push (op2 `div` op1) stack''
        runMSP prog t (stack''' , heap,symbs) methods


runMSP prog p@(Eq :t) (stack,heap,symbs) methods = 
    do  debug p (stack,heap,symbs) 
        let (op1:stack')  =  stack
        let (op2:stack'') =  stack'
        let v  = if op1 == op2 then 1 else 0
        let stack'''      =  push v stack''
        runMSP prog t (stack''' , heap , symbs) methods

runMSP prog p@(Neq :t) (stack,heap,symbs) methods = 
    do  debug p (stack,heap,symbs) 
        let (op1:stack')  =  stack
        let (op2:stack'') =  stack'
        let v  = if op1 == op2 then 0 else 1
        let stack'''      =  push v stack''
        runMSP prog t (stack''' , heap, symbs) methods


runMSP prog p@(Gt :t) (stack,heap,symbs) methods = 
    do  debug p (stack,heap,symbs) 
        let (op1:stack')  =  stack
        let (op2:stack'') =  stack'
        let v  = if op1 < op2 then 1 else 0
        let stack'''      =  push v stack''
        runMSP prog t (stack''' , heap,symbs) methods 


runMSP prog p@(Lt :t) (stack,heap,symbs) methods = 
    do  debug p (stack,heap,symbs) 
        let (op1:stack')  =  stack
        let (op2:stack'') =  stack'
        let v  = if op1 > op2 then 1 else 0
        let stack'''      =  push v stack''
        runMSP prog t (stack''' , heap,symbs) methods
\end{code}


\paragraph{Jump Instructions}


\begin{code}

runMSP prog p@(Jump l:t) state methods = 
    do  debug p state
        jmp prog l state methods

runMSP prog p@(Jumpf l:t) (stack,heap,symbs) methods = 
    do  debug p (stack,heap,symbs) 
        let (v:stack')  =  stack
        if v == 1 then runMSP prog t (stack',heap,symbs) methods
                  else jmp prog l (stack',heap,symbs) methods

runMSP prog (ALabel n:t) state methods = runMSP prog t state methods


jmp prog label state methods = runMSP prog prog' state methods
  where (Just npc) = elemIndex (ALabel label) prog
        prog'      = drop (npc+1) prog


npc :: Int -> Int -> Integer
npc l1 l2 =  toInteger' (l1 - l2)

toInteger' :: Int -> Integer
toInteger' i = read (show i)

toInt' :: Integer -> Int
toInt' i = read (show i)

\end{code}



\begin{code}

debug p (stack,heap,symbs) = return ()

debug' :: Code -> State -> IO()
debug' p (stack,heap,symbs) = 
  do  putStrLn ""
      putStrLn ("Instruction: " ++ (show $ head p))
      putStrLn ("Stack      : " ++ (show stack))
      putStrLn ("Heap       : " ++ (show heap))
--      getChar      

\end{code}



\begin{code}
-- Testing


progExpo = [Data,Var "main_1" 0 1 C_Inttype_1,Var "expo_2" 1 1 C_Realtype_1,Var "a_2" 2 1 C_Inttype_1,Var "b_2" 3 1 C_Inttype_1,Var "res_2" 4 1 C_Realtype_1,Cod,Call "main",Halt,ALabel "main",Call "expo",Ret,ALabel "expo",Pusha "a" 2,IIn,Store,Pusha "b" 2,IIn,Store,Pusha "res" 2,Pushi 1,Store,ALabel "while_1",Pusha "b" 2,Load,Pushi 0,Gt,Jumpf "e_whl_1",Pusha "res" 2,Pusha "res" 2,Load,Pusha "a" 2,Load,Mul,Store,Pusha "b" 2,Pusha "b" 2,Load,Pushi 1,Sub,Store,Jump "while_1",ALabel "e_whl_1",Pusha "res" 2,Load,IOut,Ret]



progArray = [Data
            ,Var "arr_0" 0 10 C_Inttype_1
	    ,Var "main_1" 10 1 C_Inttype_1
	    ,Var "i_1" 11 1 C_Inttype_1
	    ,Cod
	    ,Call "main"
	    ,Halt
	    ,ALabel "main"
	    ,Pusha "i" 1
	    ,Pushi 1
	    ,Store
	    ,ALabel "while_1"
	    ,Pusha "i" 1
	    ,Load
	    ,Pushi 11
	    ,Lt
	    ,Jumpf "e_whl_1"
	    ,Pusha "arr" 0
	    ,Pusha "i" 1
	    ,Load
	    ,Pushi 1
	    ,Sub
	    ,Adda
	    ,Pusha "i" 1
	    ,Load
	    ,Store
	    ,Pusha "i" 1
	    ,Pusha "i" 1
	    ,Load
	    ,Pushi 1
	    ,Add
	    ,Store
	    ,Jump "while_1"
	    ,ALabel "e_whl_1"
	    ,Pusha "i" 1
	    ,Pushi 1
	    ,Store
	    ,ALabel "while_2"
	    ,Pusha "i" 1
	    ,Load
	    ,Pushi 11
	    ,Lt
	    ,Jumpf "e_whl_2"
	    ,Pusha "arr" 0
	    ,Pusha "i" 1
	    ,Load
	    ,Pushi 1
	    ,Sub
	    ,Adda
	    ,Load
	    ,IOut
	    ,Pusha "i" 1
	    ,Pusha "i" 1
	    ,Load
	    ,Pushi 1
	    ,Add
	    ,Store
	    ,Jump "while_2"
	    ,ALabel "e_whl_2"
	    ,Ret]

progFac = [Data
          ,Var "main_1" 0 1 C_Inttype_1
	  ,Var "x_1" 1 1 C_Inttype_1
	  ,Var "f_1" 2 1 C_Inttype_1
	  ,Var "factorial_2" 3 1 C_Inttype_1
	  ,Var "n_2" 4 1 C_Inttype_1
	  ,Var "res_2" 5 1 C_Inttype_1
	  ,Cod
	  ,Call "main"
	  ,Halt
	  ,ALabel "main"
	  ,Pusha "x" 1
	  ,Pushi 4
	  ,Store
	  ,Pusha "f" 1
	  ,Pusha "n" 2
	  ,Pusha "x" 1
	  ,Load
	  ,Store
	  ,Call "factorial"
	  ,Pusha "factorial" 2
	  ,Load
	  ,Store
	  ,Pusha "f" 1
	  ,Load
	  ,IOut
	  ,Ret
	  ,ALabel "factorial"
	  ,Pusha "res" 2
	  ,Pushi 1
	  ,Store
	  ,ALabel "while_1"
	  ,Pusha "n" 2
	  ,Load
	  ,Pushi 0
	  ,Gt
	  ,Jumpf "e_whl_1"
	  ,Pusha "res" 2
	  ,Pusha "res" 2
	  ,Load
	  ,Pusha "n" 2
	  ,Load
	  ,Mul
	  ,Store
	  ,Pusha "n" 2
	  ,Pusha "n" 2
	  ,Load
	  ,Pushi 1
	  ,Sub
	  ,Store
	  ,Jump "while_1"
	  ,ALabel "e_whl_1"
	  ,Pusha "factorial" 2
	  ,Pusha "res" 2
	  ,Load
	  ,Store
	  ,Ret]


progATS = [Data,Var "main_1" 0 1 C_Inttype_1,Var "x_1" 1 1 C_Inttype_1,Var "f_1" 2 1 C_Inttype_1,Var "factorial_2" 3 1 C_Inttype_1,Var "n_2" 4 1 C_Inttype_1,Var "res_2" 5 1 C_Inttype_1,Cod,Call "main",Halt,ALabel "main",Pusha "x" 1,Pushi 4,Store,Pusha "f" 1,Pusha "n" 2,Pusha "x" 1,Load,Store,Call "factorial",Pusha "factorial" 2,Load,Store,Pusha "f" 1,Load,IOut,Ret,ALabel "factorial",Pusha "res" 2,Pushi 1,Store,ALabel "while_1",Pusha "n" 2,Load,Pushi 0,Gt,Jumpf "e_whl_1",Pusha "res" 2,Pusha "res" 2,Load,Pusha "n" 2,Load,Mul,Store,Pusha "n" 2,Pusha "n" 2,Load,Pushi 1,Sub,Store,Jump "while_1",ALabel "e_whl_1",Pusha "factorial" 2,Pusha "res" 2,Load,Store,Ret]



\end{code}

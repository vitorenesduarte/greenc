module Data_Lazy where

import LrcPrelude

--
--      Abstract Syntax
--
--


type Code = [Instr]

data Instr
	= ALabel String 
	| Add 
	| Adda 
	| And 
	| Call String
	| Cod 
	| Data 
	| Div 
	| Eq 
	| Gt 
	| Halt 
	| IIn 
	| IOut 
	| IOutC
	| Jump String
	| Jumpf String 
	| Load 
	| Lt 
	| Minus 
	| Mul 
	| Neq 
	| Not 
	| Or 
	| Pusha String !INT 
	| Pushb !BOOL 
	| Pushi !INT 
	| Pushr !REAL 
	| Ret 
	| Store 
	| Sub 
	| Var String !INT !INT !Type 
	deriving (Show , Read , Eq , Ord)


data Type
	= C_Booltype_1
	| C_Chartype_1 
	| C_Errortype_1
	| C_Inttype_1
	| C_Realtype_1 
	deriving (Show , Read , Eq , Ord)

# GreenC

### Build it!

```bash
$ make
```

---------------


### Run it!
```bash
$ sudo ./msp examples/factorialOf4.msp False charts/reads.json 
$ sudo ./msp examples/fac.c-- True charts/reads.json
$ bash start-me-a-chart.sh
```

### Stop it!
```bash
$ bash stop-me-a-chart.sh
```

---------------


### Run it with style!
```bash
$ cd server/
$ bash start.sh
$ google-chrome http://localhost:3000
```

### Stop it!
```bash
$ bash stop.sh
```

---------------



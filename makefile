msp : MainMsp.hs
	gcc -O2 -Wall -c rapl/rapl.c
	gcc -O2 -Wall -c rapl/lib.c
	ghc --make MainMsp.hs lib.o rapl.o -o msp -lm -threaded vm/MSPemulator c--/*.hs
	chmod u+x msp

clean:
	rm -f *.aux *.hi *.log *.o vm/*.hi vm/*.o c--/*.hi c--/*.o

cleanall:
	rm -f *.aux *.hi *.log *.o vm/*.hi vm/*.o ./msp c--/*.hi c--/*.o 

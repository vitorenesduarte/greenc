-- see https://github.com/bos/aeson/blob/master/examples/Generic.hs
--
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}

module ReadsToJSON where

import Data.Aeson (ToJSON, encode)
-- the following is needed because of this issue:
-- https://github.com/bos/aeson/issues/290
import Data.Aeson (toJSON, genericToJSON, defaultOptions)
import GHC.Generics (Generic)
import qualified Data.ByteString.Lazy.Char8 as BL

data JSONRead = JSONRead {
  parent :: String,
  method :: String,
  value :: Double
} deriving (Show, Generic)

data JSONReads = JSONReads {
  reads :: [JSONRead]
} deriving (Show, Generic)

instance ToJSON JSONRead where
  toJSON = genericToJSON defaultOptions

instance ToJSON JSONReads where
  toJSON = genericToJSON defaultOptions

readsToJSONFile :: FilePath -> [(String, String, Double)] -> IO ()
readsToJSONFile file l = do BL.writeFile file (encode (readsToJSONReads l)) 

readsToJSONReads :: [(String, String, Double)] -> JSONReads
readsToJSONReads l = readsToJSONReads_ l [] 

readsToJSONReads_ :: [(String, String, Double)] -> [JSONRead] -> JSONReads
readsToJSONReads_ [] l = JSONReads (reverse l)
readsToJSONReads_ (h:t) l = readsToJSONReads_ t (addJSONRead h l)

addJSONRead :: (String, String, Double) -> [JSONRead] -> [JSONRead]
addJSONRead (parent, method, value) l = (JSONRead parent method value):l


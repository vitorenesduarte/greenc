{-# LANGUAGE ForeignFunctionInterface #-}
{-# LANGUAGE BangPatterns #-}

module Main where

import Foreign
import Foreign.C.Types
import MSPemulator
import Data_Lazy
import CmmToMsp
import ReadsToJSON
import System.Environment
import Control.Concurrent

foreign import ccall unsafe "rapl/lib.h raplInit"
  raplInitC :: Int -> Int

foreign import ccall unsafe "rapl/lib.h raplMeasure"
  raplMeasureC :: Int -> Double

raplInit :: Int -> Int
raplInit = raplInitC

raplMeasure :: Int -> Double
raplMeasure = raplMeasureC

type Reads = [(String, String, Double)] -- [(methodParentName, methodName, joules)]

main :: IO ()
main =  
  do args <- getArgs
     str <- readFile $ args!!0
     let compile = (read $ args!!1) :: Bool
     let readsJSONFile = args!!2

     let progMsp = ternary compile ((cmmToMsp str), (read str) :: Msp)
     --putStrLn $ show progMsp
     let !success = raplInit 0
     
     readsMVar <- newEmptyMVar
     methodsMVar <- newEmptyMVar
     doneMspMVar <- newEmptyMVar

     let !methods = [("main", 0), ("", 0)] -- ugly and not needed. But easier than a bunch of ifs
     !p <- putMVar methodsMVar methods

     forkOS $ runMsp progMsp methodsMVar doneMspMVar

     raplLoop [] readsMVar methodsMVar doneMspMVar
     reads <- takeMVar readsMVar
     --putStrLn $ show reads
     readsToJSONFile readsJSONFile reads

raplLoop :: Reads -> MVar Reads -> MVar Methods -> MVar Bool -> IO ()
raplLoop reads readsMVar methodsMVar doneMspMVar = do isEmpty <- isEmptyMVar doneMspMVar
                                                      case isEmpty of 
                                                         False -> putMVar readsMVar (reverse reads)
                                                         True -> waitAndReddit reads readsMVar methodsMVar doneMspMVar

waitAndReddit :: Reads -> MVar Reads -> MVar Methods -> MVar Bool -> IO ()
waitAndReddit reads readsMVar methodsMVar doneMspMVar = do let !read = raplMeasureC 0
                                                           !methods <- takeMVar methodsMVar
                                                           let !method = fst (methods!!0)
                                                           let !parent = fst (methods!!1)
                                                           !p <- putMVar methodsMVar methods
                                                           threadDelay 1000
                                                           --putStrLn $ show (parent, method, read)
                                                           raplLoop ((parent, method, read):reads) readsMVar methodsMVar doneMspMVar

runMsp :: Msp -> MVar Methods -> MVar Bool -> IO ()
runMsp progMsp methodsMVar doneMspMVar = do runMspProg progMsp methodsMVar
                                            putMVar doneMspMVar True



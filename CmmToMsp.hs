module CmmToMsp where

import LrcPrelude
import Funcs_Parser_Lazy
import Data_Lazy
import Visfun_Lazy

type Msp = [Instr]

runSemantics :: [Char] -> ([Instr], [OneError], [String], [OneTypeError], [String])
runSemantics inp = lrcEval $ runParser inp

cmmToMsp :: String -> Msp
cmmToMsp code = msp where (msp, errors, msp_pp, te, c_pp) = runSemantics code


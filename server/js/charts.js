function highchart(outputFile) {
  $.getJSON(outputFile, function(json){
    console.log(json);


    var reads = json.reads;
    var methods = [];
    var parents = {};
    var lastReadsIndex = json.reads.length - 1;

    //torna os valores absolutos em valores relativos a valores anteriores e regista todos os metodos que devem ser actualizados numa dada chamada
    for(i = lastReadsIndex; i > 0; i--){
      reads[i].value -= reads[i-1].value;
      if (reads[lastReadsIndex - i].parent == ""){
        parents[reads[lastReadsIndex - i].method] = [reads[lastReadsIndex - i].method];
      }
      else{
        parents[reads[lastReadsIndex - i].method] = [reads[lastReadsIndex - i].method].concat(parents[reads[lastReadsIndex - i].parent]) ;
      }
    }
    console.log(Object.keys(parents));


    var series = [];

    for(i = 0; i < Object.keys(parents).length; i++){
      serie = {};
      serie.name = Object.keys(parents)[i];
      serie.data = [0];
      if(serie.name != ""){
        series.push(serie);
      }
    }

    for (i =1; i < reads.length ; i++) { // i corresponde a reads[i]
      console.log(reads[i-1]);
      methods[i-1] = reads[i-1].method;
      var read = reads[i].value;
      if(reads[i].method != ""){
        for(j = 0; j < series.length; j++){ // j corresponde a series[j]
          var total = 0;
          if(parents[reads[i].method]) {
            for(k = 0; k < parents[reads[i].method].length; k++){ // k corresponde a parents[series[j].name][k]
              if(parents[reads[i].method][k] == series[j].name ){
                total = read + (series[j].data)[i-1];
              }
            }
          }

          var rounded = Math.round(total * 10) / 10;
          series[j].data.push(rounded);
        }
      }
    }

    methods[lastReadsIndex] = reads[lastReadsIndex].method;

    $('#container').highcharts({
        chart: {
            type: 'area'
        },
        title: {
            text: 'Green MSP'
        },
        subtitle: {
            text: 'Chart'
        },
        xAxis: {
            categories:[]
        },
        yAxis: {
            title: {
                text: 'Acumulated Consumption (Joule)'
            }
        },
        tooltip: {
            formatter : function() {
                return '<b>Method:</b> ' + methods[this.x] + '<br/>'
                     + '<b>Value:</b> ' + this.y + ' J';
            },
            useHTML: true
        },
        plotOptions: {
          spline: {
            lineWidth: 2,
            states: {
              hover: {
                lineWidth: 5
              }
            },
            marker: {
              enabled: false
            },
            dataLabels: {
              enabled: false
            },
            enableMouseTracking: true
          }
        },
        series: series
     });
  });
}

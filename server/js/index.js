$('#run-as-msp').on('click', function(e) {
  e.preventDefault();
  run(false);
});

$('#run-as-c--').on('click', function(e) {
  e.preventDefault();
  run(true);
});

function run(compile) {
  console.log(compile);
  $('#output-div').slideUp('slow');

  var code = $('#code').val();
  var input = $('#input').val();

  $.get('/msp/run', 
    { code: code, input: input, compile: compile }, 
    function(data) {
      console.log(data);
      var output = data[0];
      var outputFile = data[1];
      $('#output').val(output);

      highchart(outputFile);
      $('#output-div').slideDown('slow');
    }
  );
}

$('#back-to-top').on('click', function(e) {
  e.preventDefault();
  $('html,body').animate({
    scrollTop: 0
  }, 700);
});

$('.do-nothing').on('click', function(e) {
  e.preventDefault();
});

$('.green-title').on('click', function(e) {
  $('#code').val('[Data,Var "main_1" 0 1 C_Inttype_1,Var "x_1" 1 1 C_Inttype_1,Var "f_1" 2 1 C_Inttype_1,Var "factorial_2" 3 1 C_Inttype_1,Var "n_2" 4 1 C_Inttype_1,Var "res_2" 5 1 C_Inttype_1,Cod,Call "main",Halt,ALabel "main",Pusha "x" 1,IIn,Store,Pusha "f" 1,Pusha "n" 2,Pusha "x" 1,Load,Store,Call "factorial",Pusha "factorial" 2,Load,Store,Pusha "f" 1,Load,IOut,Ret,ALabel "factorial",Pusha "res" 2,Pushi 1,Store,ALabel "while_1",Pusha "n" 2,Load,Pushi 0,Gt,Jumpf "e_whl_1",Pusha "res" 2,Pusha "res" 2,Load,Pusha "n" 2,Load,Mul,Store,Pusha "n" 2,Pusha "n" 2,Load,Pushi 1,Sub,Store,Jump "while_1",ALabel "e_whl_1",Pusha "factorial" 2,Pusha "res" 2,Load,Store,Ret]');
  $('#input').val('10000');
});

$('.green-subtitle').on('click', function(e) {
  var code = 
    'main() : int\n' + 
    '{ \n' +
    '   int x; \n' + 
    '   int f; \n' + 
    '   input x; \n' + 
    '   f = factorialRec (x); \n' +
    '   print f; \n' + 
    '};\n ' + 
    '\n' + 
    'factorialRec (int x) : int \n' + 
    '{ if ( x == 0) then \n' + 
    '   { factorialRec = 1;} \n' +
    '  else \n ' + 
    '   { factorialRec = x * factorialRec (x - 1); }; \n' +
    '};';
  
  $('#code').val(code);
  $('#input').val('10000');
});



var exec    = require('child_process').exec;
var fs      = require('fs');
var async   = require('async');
var express = require('express');
var app     = express();
var requestNumber = 0;

app.use(express.static(__dirname));

app.get('/msp/run', function(req, res) {
  console.log(JSON.stringify(req.query));
  requestNumber++;
  var code = req.query.code;
  var input = req.query.input;
  var compile = req.query.compile;

  console.log('/msp/run');
  console.log('code:');
  console.log(code);
  console.log('input:');
  console.log(input);
  console.log('compile:');
  console.log(compile);

  if(!code.trim()) { res.sendStatus(400); return; } 

  async.parallel([
    function(callback){
      fs.writeFile('.code', code, callback);
    },
    function(callback){
      fs.writeFile('.input', input, callback);
    }
  ],
  function() {
    var outputFile = 'reads/' + requestNumber + '.json'
    var cmd = 'sudo ./msp .code ';
    cmd += compile == 'true' ? 'True ' : 'False ';
    cmd += outputFile;

    if(input.trim()) cmd += ' < .input';

    console.log('cmd:');
    console.log(cmd);

    var options = {
      encoding: 'utf8',
      timeout: 0,
      maxBuffer: 1024*1024*1024
    };

    var msp = exec(cmd, options, function(error, stdout, stderr) {
      if(error) res.send(500);
      else res.send([stdout, outputFile]);
    });
  });
});

app.listen(3000);

